package br.com.tokio.cliente.factory;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.stereotype.Component;

import java.text.SimpleDateFormat;

/**
 * The Class FactoryConverter.
 */
@Component
public class FactoryConverter {

    /**
     * The model mapper.
     */
    @Autowired
    private ModelMapper modelMapper;

    private ObjectMapper objectMapper;

    /**
     * The Constant ERROR.
     */
    private static final String ERROR = "no.value.present";

    public FactoryConverter() {
        objectMapper = new ObjectMapper();
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_EMPTY);
        objectMapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd HH:mm:ss"));
    }


    /**
     * Convert page to response.
     *
     * @param <T>   the generic type
     * @param <E>   the element type
     * @param pages the pages
     * @param t     the t
     * @return the page
     */
    public <T, E> Page<T> convertPageToResponse(Page<E> pages, final Class<T> t) {
        if (pages != null) {
            return (Page<T>) (pages).map(it -> modelMapper.map(it, t));
        }
        return Page.empty();
    }


}


