package br.com.tokio.cliente.repository;

import br.com.tokio.cliente.entity.Cliente;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository("clienteRepositorySD")
public interface ClienteRepository extends JpaRepository<Cliente, Long> {
    Page<Cliente> findAllByAgeLessThanEqualAndAgeGreaterThanEqual(Long endAge, Long startAge, final Pageable pageable);
}
