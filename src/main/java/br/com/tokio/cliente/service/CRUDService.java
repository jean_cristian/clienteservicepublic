package br.com.tokio.cliente.service;

import org.hibernate.service.spi.ServiceException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.List;

public interface CRUDService<T, K> {

    /**
     * Save.
     *
     * @param entity the entity
     * @return the t
     * @throws ServiceException the service exception
     */
    T save(T entity) throws ServiceException;

    /**
     * Delete.
     *
     * @param entity the entity
     * @throws ServiceException the service exception
     */
    void delete(T entity) throws ServiceException;

    /**
     * Delete.
     *
     * @param id of the entity
     * @throws ServiceException the service exception
     */
    void delete(Long id) throws ServiceException;

    /**
     * Save all.
     *
     * @param entities the entities
     * @return the list
     * @throws ServiceException the service exception
     */
    List<T> saveAll(Iterable<T> entities) throws ServiceException;

    /**
     * Find by id.
     *
     * @param id the id
     * @return the t
     * @throws ServiceException the service exception
     */
    T findById(K id) throws ServiceException;

    /**
     * Find all.
     *
     * @return the iterable
     * @throws ServiceException the service exception
     */
    Iterable<T> findAll() throws ServiceException;

    /**
     * Find by filter.
     *
     * @param filter   the filter
     * @param value    the value
     * @param pageable the pageable
     * @return the page
     * @throws ServiceException the service exception
     */
    Page<T> findByFilter(String filter, String value, Pageable pageable) throws ServiceException;


}

