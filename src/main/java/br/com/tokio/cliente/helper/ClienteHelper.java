package br.com.tokio.cliente.helper;

import br.com.tokio.cliente.entity.Cliente;
import br.com.tokio.cliente.dto.request.ClienteRequest;
import org.hibernate.service.spi.ServiceException;
import org.springframework.stereotype.Component;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

@Component
public class ClienteHelper {

    public Cliente factoryCliente(final ClienteRequest request) throws ServiceException {

        try {
            final Cliente customer = new Cliente();
            customer.setBirthDate(request.getBirthDate());
            customer.setAge(calculateAge(request.getBirthDate()));
            customer.setName(request.getName());
            return customer;
        } catch (Exception e) {
            throw new ServiceException("Factory fail");
        }
    }

    private Long calculateAge(Date birthDate) throws ServiceException {
        Calendar dateOfBirth = new GregorianCalendar();
        dateOfBirth.setTime(birthDate);

        Calendar today = Calendar.getInstance();
        int age = today.get(Calendar.YEAR) - dateOfBirth.get(Calendar.YEAR);
        dateOfBirth.add(Calendar.YEAR, age);


        //se a data de hoje é antes da data de Nascimento, então diminui 1(um)
        if (today.before(dateOfBirth)) {
            age--;
        }

        return Long.valueOf(age);
    }

}
